import Maze from "./Maze.js";

export default class Kruskal extends Maze {
    calculate() {
        Maze.shuffle(this.walls)

        this.walls.forEach(wall => {
            if (this.tileFrom(wall).g != this.tileTo(wall).g) {
                wall.direction = 'x'
                const search = this.tileTo(wall).g
                const replace = this.tileFrom(wall).g
                this.tiles.forEach(tile => {
                    if (tile.g == search) {
                        tile.g = replace
                    }
                })
            }
        })

    }
}