export default class Maze {
    constructor(width, height, size, wallSize, target) {
        this.width = width
        this.height = height
        this.size = size
        this.wallSize = wallSize
        this.target = document.querySelector(target)
        this.ctx = this.target.getContext('2d')
    }

    static shuffle(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]]
        }
        return array
    }

    tileFrom(wall) {
        return this.tiles[wall.from]
    }
    tileTo(wall) {
        return this.tiles[wall.to]
    }

    createGrid() {
        this.target.width = this.width * this.size + this.wallSize
        this.target.height = this.height * this.size + this.wallSize

        this.tiles = []
        this.walls = []

        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                this.tiles.push({
                    x: x,
                    y: y,
                    g: y * this.width + x,
                    parent: null,
                    coord: {
                        x: x * this.size + this.wallSize / 2,
                        y: y * this.size + this.wallSize / 2
                    }
                })

                if (x > 0) {
                    this.walls.push({
                        from: y * this.width + x - 1,
                        to: y * this.width + x,
                        direction: 'v',
                        tileFrom: () => this.tiles[y * this.width + x - 1],
                        tileTo: () => this.tiles[y * this.width + x]
                    })
                }

                if (y > 0) {
                    this.walls.push({
                        from: (y - 1) * this.width + x,
                        to: y * this.width + x,
                        direction: 'h',
                        tileFrom: () => this.tiles[(y - 1) * this.width + x],
                        tileTo: () => this.tiles[y * this.width + x]
                    })
                }
            }
        }
    }

    resetTiles() {
        this.tiles = []

        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                this.tiles.push({
                    x: x,
                    y: y,
                    g: y * this.width + x,
                    parent: null,
                    coord: {
                        x: x * this.size + this.wallSize / 2,
                        y: y * this.size + this.wallSize / 2
                    }
                })
            }
        }
    }

    resetWalls() {
        this.walls = []

        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                if (x > 0) {
                    this.walls.push({
                        from: y * this.width + x - 1,
                        to: y * this.width + x,
                        direction: 'v',
                        tileFrom: () => this.tiles[y * this.width + x - 1],
                        tileTo: () => this.tiles[y * this.width + x]
                    })
                }

                if (y > 0) {
                    this.walls.push({
                        from: (y - 1) * this.width + x,
                        to: y * this.width + x,
                        direction: 'h',
                        tileFrom: () => this.tiles[(y - 1) * this.width + x],
                        tileTo: () => this.tiles[y * this.width + x]
                    })
                }
            }
        }
    }

    calculate() {
        throw "Must be called from child class"
    }

    removeWallsBasedOnParents() {
        this.resetWalls()
        this.walls.forEach(wall => {
            if (this.tileFrom(wall).parent == wall.to || this.tileTo(wall).parent == wall.from) {
                wall.direction = 'x'
            }
        })
    }

    draw() {
        this.createGrid()
        this.calculate()
        this.actualDraw()
    }

    actualDraw() {
        this.ctx.fillRect(0, 0, this.target.width, this.target.height)

        this.ctx.fillStyle = 'white'
        this.tiles.forEach(tile => {
            this.ctx.fillRect(tile.coord.x, tile.coord.y, this.size, this.size)
        })

        this.ctx.fillStyle = 'black'
        this.walls.forEach(wall => {
            if (wall.direction == 'v') {
                this.ctx.fillRect(this.tileTo(wall).coord.x - this.wallSize / 2, this.tileTo(wall).coord.y - this.wallSize / 2, this.wallSize, this.size + this.wallSize)
            } else if (wall.direction == 'h') {
                this.ctx.fillRect(this.tileTo(wall).coord.x - this.wallSize / 2, this.tileTo(wall).coord.y - this.wallSize / 2, this.size + this.wallSize, this.wallSize)
            }
        })

        this.ctx.fillStyle = 'black'
        this.ctx.fillRect(0, 0, this.wallSize, this.height * this.size + this.wallSize)
        this.ctx.fillRect(0, 0, this.width * this.size + this.wallSize, this.wallSize)
        this.ctx.fillRect(0, this.height * this.size, this.width * this.size + this.wallSize, this.wallSize)
        this.ctx.fillRect(this.width * this.size, 0, this.wallSize, this.height * this.size + this.wallSize)

        this.ctx.fillStyle = 'white'
        this.ctx.fillRect(this.wallSize, 0, this.size - this.wallSize, this.wallSize)
        this.ctx.fillRect((this.width - 1) * this.size + this.wallSize, this.height * this.size, this.size - this.wallSize, this.wallSize)
    }

    randomNeighbor(i) {
        const x = i % this.width
        const y = Math.floor(i / this.width)

        const neighborIds = []

        if (x > 0) {
            neighborIds.push(i - 1)
        }
        if (x < this.width - 1) {
            neighborIds.push(i + 1)
        }
        if (y > 0) {
            neighborIds.push(i - this.width)
        }
        if (y < this.height - 1) {
            neighborIds.push(i + this.width)
        }

        Maze.shuffle(neighborIds)
        return neighborIds[0]
    }
}
