import Maze from './Maze.js'

export default class Wilson extends Maze {
    calculate() {
        this.tiles[0].parent = 0
        try {
            for (let i = 1; i < this.tiles.length; i++) {
                if (this.tiles[i].parent === null) {
                    const path = this.findPath(i)
                    for (let id = 0; id < path.length - 1; id++) {
                        this.tiles[path[id]].parent = path[id + 1]
                    }
                }
            }
        } catch (e) {
            alert("Wilson algorithm not suitable for such a big maze, please change algorithm or lower the number of tiles")
        }

        this.removeWallsBasedOnParents()
    }

    findPath(i, visited = []) {
        const currentIndex = visited.indexOf(i)
        if (currentIndex > -1) {
            visited = visited.slice(0, currentIndex)
        }
        visited.push(i)

        if (this.tiles[i].parent !== null) {
            return visited
        } else {
            const random = this.randomNeighbor(i)
            return this.findPath(random, visited)
        }
    }
}