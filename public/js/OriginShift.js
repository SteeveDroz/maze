import Maze from './Maze.js'

export default class OriginShift extends Maze {
    constructor(width, height, size, wallSize, target) {
        super(width, height, size, wallSize, target)
    }
    calculate() {
        this.crawler = this.tiles[0]

        this.tiles.forEach(tile => {
            if (tile.x === 0) {
                if (tile.y === 0) {
                    tile.parent = null
                }
                else {
                    tile.parent = tile.g - this.width
                }
            } else {
                tile.parent = tile.g - 1
            }
        })

        const iterations = 100 * this.width * this.height

        for (let i = 0; i < iterations; i++) {
            this.crawl()
        }

        this.removeWallsBasedOnParents()
    }

    crawl() {
        const source = this.crawler.g
        const destination = this.randomNeighbor(source)
        this.tiles[source].parent = destination
        this.crawler = this.tiles[destination]
        this.crawler.parent = null
    }

    drawCrawler() {
        const tile = this.tiles[this.crawler.g]
        this.ctx.fillStyle = '#0802'
        this.ctx.beginPath()
        const radius = (this.size - this.wallSize) / 2
        this.ctx.ellipse(tile.coord.x + radius, tile.coord.y + radius, radius, radius, 0, 0, 2 * Math.PI)
        this.ctx.fill()
    }
}