'use strict'

import Kruskal from "./Kruskal.js";
import OriginShift from "./OriginShift.js";
import Wilson from "./Wilson.js";

let loop
const render = () => {
    clearInterval(loop)
    const width = Number(document.querySelector('#width').value)
    const height = Number(document.querySelector('#height').value)

    const size = Number(document.querySelector('#size').value)
    const wallSize = Number(document.querySelector('#wallSize').value)

    const algorithm = document.querySelector('#algorithm').value

    const maze = document.querySelector('#maze')

    const ctx = maze.getContext('2d')

    let Algorithm
    let loopable = false
    switch (algorithm) {
        case 'kruskal':
            Algorithm = Kruskal
            break

        case 'wilson':
            Algorithm = Wilson
            break

        case 'origin-shift-animated':
            loopable = true
        case 'origin-shift':
            Algorithm = OriginShift
            break

        default:
            Algorithm = () => { }
    }

    const algo = new Algorithm(width, height, size, wallSize, '#maze')
    algo.draw()

    if (loopable) {
        loop = setInterval(() => {
            algo.crawl()
            algo.removeWallsBasedOnParents()
            algo.actualDraw()
            algo.drawCrawler()
        }, 100)
    }
}

document.querySelector('#render').addEventListener('click', render)

render()
